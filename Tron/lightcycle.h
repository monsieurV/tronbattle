#ifndef LIGHTCYCLE_H
#define LIGHTCYCLE_H

#include <vector>
#include <string>
#include <istream>
#include <ostream>
#include <sstream>
#include <stdlib.h>
#include "Board/direction.h"
#include "Board/coordinate.h"
#include "Board/gameboard.h"

namespace Tron
{
class LightCycle
{
public:
    short id;
    bool loose;
    short** board;
    std::vector<Board::Coordinate> lightCycle;
    Board::Direction orientation;
public:
    LightCycle(short _id, short **_board, int width, int height, int x = 0, int y = 0);

    void move (Board::Direction);
    std::string toString () const;
    bool initPosition(int x, int y);
};
} // namespace Tron

#endif // LIGHTCYCLE_H
