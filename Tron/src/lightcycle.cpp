#include "../lightcycle.h"

#include <iostream>

namespace Tron
{
LightCycle::LightCycle(short _id, short **_board, int width, int height, int x, int y) // tmp init position to what you want
    : board(_board), id(_id), loose(false)
{
    while (_board[y][x] != -1) {
        x = rand()%width;
        y = rand()%height;
    }

    _board[y][x] = this->id;
    Board::Coordinate position(x, y);
    this->lightCycle.push_back(position);
}

void LightCycle::move(Board::Direction dir)
{
    Board::Coordinate newPosition = this->lightCycle.back() + dir;
    this->lightCycle.push_back(newPosition);
}

std::string LightCycle::toString () const
{
    std::stringstream s;

    if (this->loose) {
        s << "-1 -1 -1 -1";
    }
    else {
        s << (this->lightCycle[0].x - 1) << " " << (this->lightCycle[0].y - 1) << " " << (this->lightCycle.back().x - 1) << " " << (this->lightCycle.back().y - 1); // tmp position - 1 ??
    }

    return s.str();
}

bool LightCycle::initPosition (int x, int y)
{
    if (this->board[y][x] == -1 && this->lightCycle.size() == 1) {
        this->board[this->lightCycle[0].y][this->lightCycle[0].x] = -1;
        this->lightCycle[0].x  = x;
        this->lightCycle[0].y  = y;
        this->board[y][x] = this->id;
        return true;
    }
    else {
        return false;
    }
}
} // namespace Tron
