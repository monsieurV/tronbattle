#ifndef SQUARE_H
#define SQUARE_H

#include "ostream"
#include "sstream"

namespace Board
{
class Square
{
public:
    Square();

    friend std::ostream& operator << (std::ostream&, Square const&);

    std::string toString () const;
};
} // namespace Board

#endif // SQUARE_H
