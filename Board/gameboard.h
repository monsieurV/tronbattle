#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <ostream>
#include <sstream>
#include "Board/coordinate.h"

namespace Board
{
template<typename T>
class GameBoard
{
public: // tmp -> protected
    T** content;
    int width;
    int height;

public:
    GameBoard (int = 0, int = 0);
    GameBoard (GameBoard<T> const&);
    ~GameBoard ();

    GameBoard<T>& operator = (GameBoard<T> const&);
    T operator () (int, int) const;
    T operator () (int, int, T);
    T operator () (Coordinate) const;
    T operator () (Coordinate, T);

    template<typename T2>
    friend std::ostream& operator << (std::ostream&, GameBoard<T2> const&);

    std::string toString () const;

private:
    void copyBoardContent (GameBoard const&);
};

template<typename T>
GameBoard<T>::GameBoard (int _width, int _height)
    :width(_width), height(_height), content(NULL)
{
    if (this->width > 0 && this->height > 0) {
        this->content = new T*[this->height];

        for (int y = 0; y < this->height; ++y) {
            this->content[y] = new T[this->width];
        }
    }
    else {
        this->width = 0;
        this->height = 0;
    }
}

template<typename T>//tmp ( constructeur par copie )
GameBoard<T>::GameBoard (GameBoard<T> const& obj)
    :content(NULL)
{
    this->copyBoardContent(obj);
}

template<typename T>
GameBoard<T>::~GameBoard ()
{
    if (this->content) {
        for (int y = 0; y < this->height; ++y) {
            delete[] this->content[y];
        }

        delete[] this->content;
    }
}

template<typename T>
GameBoard<T>& GameBoard<T>::operator = (GameBoard<T> const& obj)
{
    if (this->content) {
        this->~GameBoard();
    }

    this->copyBoardContent(obj);

    return *this;
}

template<typename T>
T GameBoard<T>::operator () (int x, int y) const
{
    return this->content[y][x];
}

template<typename T>
T GameBoard<T>::operator () (int x, int y, T value)
{
    this->content[y][x] = value;

    return value;
}

template<typename T>
T GameBoard<T>::operator () (Coordinate coord) const
{
    return this->content[coord.y][coord.x];
}

template<typename T>
T GameBoard<T>::operator () (Coordinate coord, T value)
{
    this->content[coord.y][coord.x] = value;

    return value;
}

template<typename T2>
std::ostream& operator << (std::ostream& stream, GameBoard<T2> const& obj)
{
    stream << obj.toString();

    return stream;
}

template<typename T>
std::string GameBoard<T>::toString () const
{
    std::stringstream s;

    for (int y = 0; y < this->height; ++y) {
        for (int x = 0; x < this->width; ++x) {
            s << this->content[y][x] << " ";
        }
        s << std::endl;
    }

    return s.str();
}

template<typename T>
void GameBoard<T>::copyBoardContent (GameBoard<T> const& obj)
{
    this->width = obj.width;
    this->height = obj.height;

    if (obj.content) {
        this->content = new T*[this->height];

        for (int y = 0; y < this->height; ++y) {
            this->content[y] = new T[this->width];

            for (int x = 0; x < this->width; ++x) {
                this->content[y][x] = obj.content[y][x];
            }
        }
    }
}
} // namespace Board

#endif // GAMEBOARD_H
