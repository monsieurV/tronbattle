#-------------------------------------------------
#
# Project created by QtCreator 2014-02-05T11:17:32
#
#-------------------------------------------------

QT       += core gui \
         xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tronBattle_g
TEMPLATE = app

SOURCES += main.cpp \
    mainwindow.cpp \
    processmanager/src/processmanager.cpp \
    computer.cpp \
    trongame.cpp \
    Board/src/coordinate.cpp \
    Board/src/direction.cpp \
    Board/src/square.cpp \
    Tron/src/lightcycle.cpp \
    tronround.cpp \
    playerwidget.cpp \
    selectprogramdialog.cpp


HEADERS += \
    mainwindow.h \
    processmanager/processmanager.h \
    computer.h \
    trongame.h \
    Board/Board \
    Board/coordinate.h \
    Board/direction.h \
    Board/gameboard.h \
    Board/square.h \
    Tron/lightcycle.h \
    tronround.h \
    playerwidget.h \
    selectprogramdialog.h

FORMS    +=

TRANSLATIONS = tronBattle_g_fr.ts tronBattle_g_en.ts



