<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="mainwindow.cpp" line="7"/>
        <source>Tron AI battle emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>&amp;File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <source>&amp;Exit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="53"/>
        <source>&amp;Game</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="30"/>
        <source>&amp;New Battle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="36"/>
        <source>&amp;Open Battle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="42"/>
        <source>&amp;Save Battle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="54"/>
        <source>&amp;Generate Battle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source> timeout : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source> sleep time : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="72"/>
        <source> continue :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="76"/>
        <source> zoom : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="100"/>
        <source>Save Battle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="236"/>
        <source>Open Battle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <location filename="mainwindow.cpp" line="702"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <location filename="mainwindow.cpp" line="702"/>
        <source>Program &quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <source>&quot; does not exist !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="702"/>
        <source>&quot; is not executable !</source>
        <translation></translation>
    </message>
    <message>
        <location filename="playerwidget.cpp" line="17"/>
        <source>Is script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="playerwidget.cpp" line="20"/>
        <source>scriptInterpreter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="playerwidget.cpp" line="25"/>
        <source>random position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="playerwidget.cpp" line="82"/>
        <source>Choose your AI</source>
        <oldsource>Choose tour AI</oldsource>
        <translation></translation>
    </message>
</context>
</TS>
