#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QColor>
#include <QString>
#include <QCheckBox>
#include <QSpinBox>
#include <QColorDialog>
#include <QFileDialog>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <sstream>

class PlayerWidget : public QWidget
{
    Q_OBJECT
public:
    QPushButton *programButton;
    QPushButton *colorButton;
    QCheckBox *randomPosition;
    QSpinBox *posX;
    QSpinBox *posY;
    QCheckBox *isScript;
    QLineEdit *script;

    QColor color;
    QString programDirectory;
    QString programName;
    short playerId;

public:
    explicit PlayerWidget (short _playerId, QColor _color, QString _program = "./assets/prog/default.easy", QWidget *parent = 0);
    void setPlayerColor (QColor const& color);
    void disable (bool disable);
    void setProgramName(QString program);
signals:
    void colorChanged(QColor const&, short);
    void positionChanged ();
public slots:
    void selectColor ();
    void selectProgram ();
    void setRandom (bool random);
    void toggleIsScript (bool isScript);
    void setPositionBackgroundColor (QColor color = QColor(255, 255, 255));
};

#endif // PLAYERWIDGET_H
