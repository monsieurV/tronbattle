#ifndef COMPUTER_H
#define COMPUTER_H

#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <signal.h>
#include <vector>
#include <string>
#include <sstream>
#include "processmanager/processmanager.h"
#include "Board/direction.h"
#include "Tron/lightcycle.h"

class Computer
{
private:
    pid_t pid;
    int input_fd;
    int output_fd;
    int err_fd;
    FILE *input_stream; //tmp ??
    FILE *output_stream; // tmp ??
    FILE *err_stream; // tmp ??
public:
    Computer (char **arg_list);
    ~Computer(); // tmp

    Board::Direction getNextMovement (short playerNumber, short playerId, Tron::LightCycle** playersInfo, std::string *stdout, std::string *stderr, std::string *lostMessage, int timeout = 100);
};

#endif // COMPUTER_H
