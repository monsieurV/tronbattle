#include "playerwidget.h"

PlayerWidget::PlayerWidget(short _playerId, QColor _color, QString program, QWidget *parent)
    : QWidget(parent), playerId(_playerId)
{
    QVBoxLayout *layout = new QVBoxLayout (this);
    QHBoxLayout *buttonLayout = new QHBoxLayout();
        this->programButton = new QPushButton();
        this->setProgramName(program);
        buttonLayout->addWidget(this->programButton);
        this->colorButton = new QPushButton();
        this->setPlayerColor(_color);
        buttonLayout->addWidget(this->colorButton);
    layout->addLayout(buttonLayout);
    QObject::connect(this->programButton, SIGNAL(clicked()), this, SLOT(selectProgram()));
    QObject::connect(this->colorButton, SIGNAL(clicked()), this, SLOT(selectColor()));
    this->isScript = new QCheckBox(QObject::tr("Is script"));
    this->isScript->setChecked(false);
    layout->addWidget(this->isScript);
    this->script = new QLineEdit(QObject::tr("scriptInterpreter"));
    this->toggleIsScript(false);
    layout->addWidget(this->script);
    QObject::connect(this->isScript, SIGNAL(toggled(bool)), this, SLOT(toggleIsScript(bool)));

    this->randomPosition = new QCheckBox(QObject::tr("random position"));
    layout->addWidget(this->randomPosition);
    QHBoxLayout* posLayout = new QHBoxLayout();
        this->posX = new QSpinBox();
        this->posX->setRange(1, 30);// tmp hardcode
        QLabel* x = new QLabel("x");
        QObject::connect(this->posX, SIGNAL(valueChanged(int)), this, SIGNAL(positionChanged()));
        this->posY = new QSpinBox();
        this->posY->setRange(1, 20);// tmp hardcode
        QObject::connect(this->posY, SIGNAL(valueChanged(int)), this, SIGNAL(positionChanged()));
        QLabel* y = new QLabel("y");
        posLayout->addWidget(x);
        posLayout->addWidget(this->posX);
        posLayout->addWidget(y);
        posLayout->addWidget(this->posY);
        layout->addLayout(posLayout);
    QObject::connect(this->randomPosition, SIGNAL(toggled(bool)), this, SLOT(setRandom(bool)));
    this->randomPosition->setChecked(true);
    this->setMaximumHeight(200);
}

void PlayerWidget::setProgramName(QString program)
{
    int i;
    for (i = program.length() - 1; i >= 0 && program[i] !=  QChar('/'); i--);
    this->programName = program.mid(i + 1);
    this->programDirectory = program.mid(0, i);
    this->programButton->setText(this->programName);
}

void PlayerWidget::setPlayerColor (QColor const& _color)
{
    std::stringstream styleSheet;
    this->color = _color;
    styleSheet << "background-color: rgb(" << _color.red() << ", " << _color.green() << ", " << _color.blue() << ");";
    this->colorButton->setStyleSheet(styleSheet.str().c_str());
    emit colorChanged(this->color, this->playerId);
}

void PlayerWidget::disable(bool disable)
{
    this->programButton->setDisabled(disable);
    this->randomPosition->setDisabled(disable);
    this->isScript->setDisabled(disable);
    this->script->setDisabled(disable || !this->isScript->isChecked());
    posX->setDisabled(disable || this->randomPosition->isChecked());
    posY->setDisabled(disable || this->randomPosition->isChecked());
}

void PlayerWidget::selectColor ()
{
    QColor newColor = QColorDialog::getColor(this->color, this);
    if(newColor.isValid()) {
        this->setPlayerColor(newColor);
    }
}

void PlayerWidget::selectProgram ()
{
    QString program = QFileDialog::getOpenFileName(this, QObject::tr("Choose your AI"), this->programDirectory);
    if (!program.isEmpty()) {
        this->setProgramName(program);
    }
}

void PlayerWidget::setRandom (bool random)
{
    if (random) {
        this->posX->setEnabled(false);
        this->posY->setEnabled(false);
    }
    else {
        this->posX->setEnabled(true);
        this->posY->setEnabled(true);
    }

    emit this->positionChanged();
}

void PlayerWidget::toggleIsScript(bool isScript)
{
    this->script->setEnabled(isScript);
}

void PlayerWidget::setPositionBackgroundColor (QColor _color)
{
    std::stringstream styleSheet;
    styleSheet << "background-color: rgb(" << _color.red() << ", " << _color.green() << ", " << _color.blue() << ");";

    this->posX->setStyleSheet(styleSheet.str().c_str());
    this->posY->setStyleSheet(styleSheet.str().c_str());
}
