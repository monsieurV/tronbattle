#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QString>
#include <QLocale>

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <time.h>
#include "trongame.h"

const char* program_name;

void printHelp (FILE* stream, int exit_code, char* errorMessage = "") //tmp, to update
{
    printf("\n");

    if (exit_code && errorMessage != "") {
        fprintf (stream, "Error: %s\n\n", errorMessage);
    }

    fprintf (stream,
             "Use : %s options [program1 [program2 [program3 [program4]]]]\n\n", program_name);
    fprintf (stream,
             "\tThis program emulate a tron battle using external program as AI.\n"
             "\tPrograms will be order as the parameters.\n"
             "\tIf you want to use a default player, there is two possibility:\n"
             "\t\t- Specified a program name as \"DEFAULT\" or \"default\"\n"
             "\t\t- Force a number of player higher than the number of specified programs using -n option.\n"
             "\n");
    fprintf (stream,
             "OPTIONS:\n\n"
             "\t-c --continue                If continue is specified, the game will continue even if there is one player remaining.\n"
             "\t                                Of course, the game automatically continue if there is only one player.\n"
             "\t-d --difficulty difficulty   The difficulty of the default player(s) (Default value = \"easy\").\n"
             "\t                                Difficulty can take differente values : \"easy\" || \"e\" or \"medium\" || \"m\" or \"hard\" || \"h\"\n"
             "\t-g --graphical               Run program with graphic UI."
             "\t-h --help                    Display the help.\n"
             "\t-n --number playerNumber     Force the number of player to playerNumber value (The default value is the number of specified programs).\n"
             "\t                                Optional if the number of program is equal to the desired player number.\n"
             "\t                                Player number must be between 1 and 4 included.\n"
             "\t                                If the player number is higher than the number of specified programs, the remaining player will be Default player.\n"
             "\t-s --sleep time              Is specified, the programme sleep the amount of millisecond specified by time value between each AI call (default = 20 ms).\n"
             "\t                                It must be a value between 0 and 2000 ms\n"
             "\t-t --timeout time            The maximum time in miliseconde the AI programe have to answer (default = 100 ms).\n"
             "\t                                It must be a value between 1 and 10000 ms\n"
             "\n\n");

    exit (exit_code);
}

int main (int argc, char* argv[]) // tmp use a program too for the "server"
{
    int next_option;
    const char* const short_options = "cd:ghn:s:t:"; // tmp to update (-g --graphical) // tmp -v --verbose ??
    const struct option long_options[] = {
                { "continue",   0, NULL, 'c' },
                { "difficulty", 1, NULL, 'd' },
                { "graphical",  0, NULL, 'g' },
                { "help",       0, NULL, 'h' },
                { "number",     1, NULL, 'n' },
                { "sleep",      1, NULL, 's' },
                { "timeout",    1, NULL, 't' },
                { NULL,         0, NULL, 0   }
    };

    bool graphical = true; // tmp
    short playerNumber = 0;
    std::string difficulty = "easy";
    bool continueGame = false;
    int sleepTime = 20;
    int timeout = 100;
    program_name = argv[0];

    do {
        next_option = getopt_long (argc, argv, short_options,
        long_options, NULL);
        switch (next_option)
        {
        case 'c': // -c or --continue
                continueGame = true;
            break;
        case 'd': // -d or --difficulty
            if (!strcmp(optarg, "easy") || !strcmp(optarg, "e")) {
                difficulty = "easy";
            }
            else if (!strcmp(optarg, "medium") || !strcmp(optarg, "m")) {
                difficulty = "medium";
            }
            else if (!strcmp(optarg, "hard") || !strcmp(optarg, "h")) {
                difficulty = "hard";
            }
            else {
                printHelp(stderr, 1, "Invalide difficulty.");
            }

            break;
        case 'h': // -h or --help
            printHelp (stdout, 0);
        case 'g': //_g or --graphical
            graphical = true;
            break;
        case 'n': // -n or --number
            playerNumber = atoi(optarg);
            if (playerNumber < 1) {
                printHelp(stderr, 1, "Player number must be a number between 1 and 4.");
            }
            if (playerNumber > 4) {
                printHelp(stderr, 1, "Too much players. The maximum player number is 4.");
            }

            break;
        case 's': // -s or --sleep
                sleepTime = atoi(optarg);
                if (sleepTime < 1 or sleepTime > 2000) {
                    printHelp(stderr, 1, "Invalid sleep time.");
                }
            break;
        case 't': // -t or --timeout
                timeout = atoi(optarg);
                if (timeout < 0 or timeout > 10000) {
                    printHelp(stderr, 1, "Invalid timeout value.");
                }
            break;
        case '?':
            printHelp (stderr, 1, "Invalid option.");
        case -1:// end of options
            break;
        default: // Unexpected
            printf("unexpected");
            abort ();
        }
    }
    while (next_option != -1);

    int programNumber = (argc - optind);

    playerNumber = (playerNumber > 0) ? playerNumber : programNumber;

    if (playerNumber == 0 && !graphical) {
        printHelp(stderr, 1, "No player number and no program specified.");
    }

    if (programNumber > playerNumber || playerNumber > 4) {
        printHelp(stderr, 1, "To much program arguments specified. Maximum program number is 4.");
    }

    if (playerNumber == 1) {
        continueGame = true;
    }

    char ***programs_arg_lists = NULL;
    programs_arg_lists = new char**[playerNumber];

    for (int i = 0; i < playerNumber; ++i) {
        programs_arg_lists[i] = new char*[2];
        programs_arg_lists[i][0] = new char[255];

        if (i >= programNumber ||  !strcmp(argv[i + optind], "DEFAULT") || !strcmp(argv[i + optind], "default")) {
            std::stringstream s;
            s << "./assets/prog/default." << difficulty;
            strcpy(programs_arg_lists[i][0], s.str().c_str());// tmp ??
        }
        else {
            strcpy(programs_arg_lists[i][0], argv[i + optind]);
        }

        if(!std::ifstream(programs_arg_lists[i][0])) { // tmp free memory
            char message[64] = "Cannot find ";
            printHelp(stderr, 1, strcat(message, programs_arg_lists[i][0]));
        }

        programs_arg_lists[i][1] = NULL;
    }

    // tmp program
    srand(time(NULL));
    TronGame* game;
    if(!graphical) {
        game = new TronGame(30, 20, playerNumber, programs_arg_lists, continueGame, sleepTime, timeout); //tmp
        game->play();
    }
    else {

        QApplication app(argc, argv);

        QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
        QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

        QDir::setCurrent(QCoreApplication::applicationDirPath());

        //QString locale = QLocale::system().name().section('_', 0, 0); // tmp language
        QTranslator translator;
        translator.load(QString("./assets/lang/tronBattle_g_en")/* + locale*/);
        app.installTranslator(&translator);

        MainWindow w;
        w.initGame(playerNumber, programs_arg_lists, sleepTime, timeout);
        w.show();
        //w.setFixedSize(w.size()); // tmp

        return app.exec(); // tmp free memory
    }

    for (int i  = 0; i < playerNumber; ++i) {
        for (int j = 0; programs_arg_lists[i][j] != NULL; ++j) {
            delete[] programs_arg_lists[i][j];
        }
        delete[] programs_arg_lists[i];
    }
    delete[] programs_arg_lists;

    delete game;

    return 0;
}
