#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int spawn (char* program, char ** arg_list);
int spawn_with_mapped_io (char* program, char ** arg_list, int* input_fd, int* output_fd, int* err_fd);
bool readWithTimeOut (int  fd, int timeout, char* buffer, int size);// tmp don't do that here

#endif // PROCESSMANAGER_H
