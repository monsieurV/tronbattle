#include "../processmanager.h"
#include <stdio.h>

int spawn (char* program, char ** arg_list)
{
    pid_t pid;
    pid = fork ();
    if (pid == (pid_t) 0) {
        execvp (program, arg_list);
        fprintf (stderr, "An error occurred in execvp.\n"); // tmp message
        abort ();
    }
    else {
        return pid;
    }
}

int spawn_with_mapped_io (char* program, char ** arg_list, int* input_fd, int* output_fd, int *err_fd) // tmp sortie d'erreur standard stderr ???
{
    int child_input_fds[2],
        child_output_fds[2],
        child_err_fds[2];
    pid_t pid;

    pipe (child_input_fds);
    assert(child_input_fds != NULL);
    pipe (child_output_fds);
    assert (child_output_fds != NULL);
    pipe (child_err_fds);
    assert (child_err_fds != NULL);

    pid = fork ();

    if (pid == (pid_t) 0) {
        close (child_input_fds[1]);
        close (child_output_fds[0]);
        close (child_err_fds[0]);
        dup2 (child_input_fds[0], STDIN_FILENO);
        dup2 (child_output_fds[1], STDOUT_FILENO);
        dup2 (child_err_fds[1], STDERR_FILENO);
        execvp (program, arg_list);
        fprintf (stderr, "An error occurred in execvp.\n"); // tmp message
        abort ();
    }
    else {
        close (child_input_fds[0]);
        close (child_output_fds[1]);
        close (child_err_fds[1]);
        *input_fd = child_output_fds[0];
        *output_fd = child_input_fds[1];
        *err_fd = child_err_fds[0];
        return pid;
    }
}

bool readWithTimeOut (int  fd, int timeout, char* buffer, int size) // tmp don't do that here
{
    fd_set input_set;
    int readyForReading = 0;
    int readBytesNumber = 0;
    struct timeval _timeout;
    _timeout.tv_sec = (__time_t) (timeout / 1000);
    _timeout.tv_usec = (timeout % 1000) * 1000;

    FD_ZERO(&input_set );
    FD_SET(fd, &input_set);

    readyForReading = select(fd + 1, &input_set, NULL, NULL, &_timeout);

    if (readyForReading == -1) {
        fprintf (stderr, "Unable to read input.\n"); // tmp message//tmp Some error has occured in input
        //abort ();// usefull abort ??
        return false;
    } else {
        if (readyForReading) {
            FILE* file = fdopen(dup(fd), "r");
            if (file != NULL) {
                int i = 0;
                //readBytesNumber = read(fd, buffer, size);
                //buffer[readBytesNumber - 1] = '\0';
                do {
                    buffer[i] = fgetc(file);
                    i++;
                }
                while (buffer[i - 1] != EOF && i < size && buffer[i - 1] != '\n');
                buffer[i - 1] = '\0';

                fclose(file);
                return true;
            }
        }

        return false;
    }
}
