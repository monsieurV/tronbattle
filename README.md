# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is a soft that manage a tron AI battle.
* I created this soft to help me on this [codingame.com contest](https://www.codingame.com/leaderboards/challenge/20/global) because it was too hard to code and make some test directly on the codingame website.
* You can found the AI i make for the contest [here](https://bitbucket.org/monsieurV/tron_ai)

### How do I get set up? ###

* First you need to install QT 4 >= 4.8.
* You can just download the [linux x64 version](https://bitbucket.org/monsieurV/tronbattle/downloads/tronBattle_linux_x64.tar.gz) or compile it.
* If you compile it, you will need the [assets](https://bitbucket.org/monsieurV/tronbattle/downloads/assets.tar.gz) that contain some icons and some demo AI (compiled for linux only).
* There is no windows version because this soft use some unix system libraries (for pipe).

### How do I use it? ###

* The goal of this soft is to generate battle between 2 or more tron AI using standard input / output stream to communicate.
* The AI is a compiled program or a script that respect some rules you can find [here](RULES.md).

* In order to create a new battle configuration, click on the "New Battle" button (Ctrl+N)on the top menu.
* On the right side of the program, you can manage the Tron IA. You can change : 
    - The number of IA fighting (1 to 4)
    - The color of each IA
    - The starting position of each IA (Coordinate or Random)
    - The program / script of each IA.
    - If the IA is a script, you may check the "is script" checkbox and specified the script interpreter command (for exemple : "python", "java -jar", "php", ...)
* On the top menu, you can configure :
    - The timeout (in ms) : the maximum answer time for each IA.
    - The continue checkbox : If checked, the winner continue to play until his own death.
* Once the battle is configured, you can generate the battle by clicking the "Generate Battle" button (Ctrl+G) on the top menu.

* To show the generated battle, click on the play button (Ctrl+P). (You can pause/unpause by doing the same)
* You can show the battle frame by frame using the left and right key.
* On the bottom side you can see the console. For each IA answer, you can see :
    - the standard output stream of the IA (the answer of the IA)
    - the standard error stream of the IA (usefull in order to debug your IA)
    - Optionally : A message that explain why the IA loose (unknow keyword, bad movement, time out, ....)
* You can regenerate a battle starting on the current position by clicking on the "regenerate end of battle" button (Ctrl+R).
* You can change the delay between each frame by changing the "sleep time" (in ms) parameter
* You can change the display size by changing the Zoom parameter.
* You can save a generated battle (or by the way load a previously saved battle).

* If you want to generate a new battle with another configuration, click on the "New Battle" Button (Ctrl+N).