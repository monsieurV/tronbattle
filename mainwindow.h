#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui> // tmp
#include <sstream>
#include <sys/stat.h>
#include "playerwidget.h"
#include "trongame.h"
#include "tronround.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    QWidget *centralArea;
    QHBoxLayout *centralLayout;
    QVBoxLayout *gameLayout;
    QTextEdit *console;
    QHBoxLayout *sliderLayout;
    QSlider *gameViewSlider;
    QLabel *sliderLabel;
    QGraphicsScene *scene;
    QGraphicsView *view;
    TronGame *game;
    int currentRound;
    QList<QGraphicsLineItem*> ligthCycles;
    QList<QGraphicsLineItem*> PlayersLigthCycles[4];
    QWidget *playersWidget;
    QHBoxLayout *playerManagerLayout;
    PlayerWidget* players[4];
    QPushButton* addPlayerButton;
    QPushButton* removePlayerButton;
    QPushButton *startButton;
    QPushButton *regenerateButton;
    QTimer *timer;
    QAction *NewBattleAction;
    QAction *OpenAction;
    QAction *SaveAction;
    QAction *GenerateBattleAction;
    QCheckBox *continueGame;
    QSpinBox *timeout;
    QSpinBox *sleeptime;
    bool playing;
    bool isInBattle;
    short playerNumber;
    double scale;
    std::vector<TronRound> rounds;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void onSliderChanged (int);
    void onPlayButtonClicked ();
    void play ();
    void onColorChanged (QColor const&color, short id);
    void addPlayer ();
    void removePlayer ();
    void newBattle ();
    void enableBattleSettings ();
    void generateBattle ();
    void regenerateBattle ();
    void initGame (short _playerNumber, char ***programs, int _sleepTime, int _timeout);
    void save ();
    void open ();
    void zoom (int percent);
    bool doGeneration (std::vector<TronRound> *_rounds = NULL);
    void onPlayerPostionChanged ();
private:
    bool checkPlayerPosition ();
    void initCentralArea ();
    void initMenu ();
    void initGameMenu ();
    void initGameScene ();
    void cleanGameScene ();
    QString getBGColorStyleSheet (QColor color);
    void initConsole ();
    void setDefaultConsoleStyle ();
    void initSlider ();
    void initTimer ();
    void initPlayerMenu ();
    void setSliderLabel(int);
    void setPlaying (bool _playing);
    void setInBattle ();
    void clearGame ();
    void displayRound(int round, int currentRound);
    void disablePlayersWidget (bool disable);
    void setPlayerNumber (short _playerNumber);
};

#endif // MAINWINDOW_H
