#ifndef TRONGAME_H
#define TRONGAME_H

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include "computer.h"
#include "Board/coordinate.h"
#include "Board/direction.h"
#include "Tron/lightcycle.h"
#include "Board/gameboard.h"
#include "tronround.h"

class TronGame
{
public:
    short playerNumber;
    short playerRemaining;
    Computer **players;
    Tron::LightCycle **lightCycles;
    Board::GameBoard<short> *board;
    std::vector<TronRound> rounds;
    bool continueGame;
    int sleepTime;
    int timeout;
    bool verbose;

    std::string stdout;
    std::string lostMessage;
    std::string stderr;
public:
    TronGame (int _width, int _height, short _playerNumber, char ***programs, bool _continueGame = false, int _sleepTime = 100, int _timeout = 100, bool _verbose = true);
    ~TronGame();
    std::vector<TronRound> play (std::vector<TronRound> *_rounds = NULL);
    bool doLap (int i = 0);
    void doFakeRound (TronRound round);
    void display (int playerId);
    void initStdMessage();
};

#endif // TRONGAME_H
