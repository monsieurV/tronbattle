#include "computer.h"

Computer::Computer (/*std::string program*/ char** arg_list)
{    
    this->pid = spawn_with_mapped_io(arg_list[0], arg_list, &this->input_fd, &this->output_fd, &this->err_fd);//tmp
}

Computer::~Computer () {
    close (this->input_fd);
    close (this->output_fd);
    close (this->err_fd);
    kill(this->pid, SIGTERM);
    waitpid (pid, NULL, 0);
}

Board::Direction Computer::getNextMovement (short playerNumber, short playerId, Tron::LightCycle** playersInfo, std::string *stdout, std::string *stderr, std::string *lostMessage, int timeout) // tmp gestion des erreur (time out, not a valid direction direction)
{
    char nextDirection[64];
    bool readInTime;
    Board::Direction dir;

    this->output_stream = fdopen(dup(this->output_fd), "w");
    fprintf(this->output_stream, "%d %d\n", playerNumber, playerId);
    for (int i = 0; i < playerNumber; ++i) {
        fprintf(this->output_stream, "%s\n", playersInfo[i]->toString().c_str());
    }

    fflush (this->output_stream);

    fclose(this->output_stream);
    readInTime = readWithTimeOut(this->input_fd, timeout, nextDirection, 63);

    if (readInTime) {

        (*stdout) = nextDirection;
        dir = (Board::Direction) nextDirection;

        if (dir == Board::FIXED) {
            (*lostMessage) = "ERROR: This is not a valid direction.\n";
        }
    }
    else {
        std::stringstream s;
        s << "ERROR: Time out (" << timeout << "ms max).\n";
        (*lostMessage) = s.str();
    }

    fd_set input_set;//tmp don't do that here
    int readyForReading = 0;
    int readBytesNumber = 0;
    struct timeval _timeout;
    _timeout.tv_sec = 0;
    _timeout.tv_usec = 1;

    FD_ZERO(&input_set );
    FD_SET(this->err_fd, &input_set);
    readyForReading = select(this->err_fd + 1, &input_set, NULL, NULL, &_timeout);
    if (readyForReading) {
        char buffer[100000];//tmp buffer
        readBytesNumber = read(this->err_fd, buffer, 99999);
        buffer[readBytesNumber] = '\0';
        *(stderr) += buffer;
    }// end tmp don't do that here

    return dir;
}
