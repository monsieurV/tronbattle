#include "tronround.h"

TronRound::TronRound(short id, Board::Coordinate _departure, Board::Direction _movement, std::string _stdout, std::string _stderr, std::string _lostMessage, bool _justLost)
    :playerId(id), departure(_departure), arrival(departure + _movement), movement(_movement), stdout(_stdout), stderr(_stderr), lostMessage(_lostMessage), justLost(_justLost)
{
}
