#include "mainwindow.h"
#include <QtXml/QtXml>
#include <stdio.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), game(NULL), playing(false), playerNumber(1)//tmp playerNumber
{
    this->setWindowTitle(QObject::tr("Tron AI battle emulator"));
    this->initMenu();
    this->initCentralArea();
    this->enableBattleSettings();
}

MainWindow::~MainWindow()
{

}

void MainWindow::initCentralArea ()
{
    this->centralArea = new QWidget;
    this->centralLayout = new QHBoxLayout(this->centralArea);
    this->initGameMenu();
    this->initPlayerMenu();
    this->setCentralWidget(this->centralArea);
}

void MainWindow::initMenu () // tmp pointeur
{
    QMenu *FileMenu = menuBar()->addMenu(QObject::tr("&File"));
        this->NewBattleAction = new QAction(QObject::tr("&New Battle"), this);
        this->NewBattleAction->setIcon(QIcon("./assets/img/new battle.png"));
        this->NewBattleAction->setShortcut(QKeySequence("Ctrl+N"));
        connect(this->NewBattleAction, SIGNAL(triggered()), this, SLOT(newBattle()));
        FileMenu->addAction(this->NewBattleAction);

        this->OpenAction = new QAction(QObject::tr("&Open Battle"), this);
        this->OpenAction->setIcon(QIcon("./assets/img/open button.png"));
        this->OpenAction->setShortcut(QKeySequence("Ctrl+O"));
        connect(this->OpenAction, SIGNAL(triggered()), this, SLOT(open()));
        FileMenu->addAction(this->OpenAction);

        this->SaveAction = new QAction(QObject::tr("&Save Battle"), this);
        this->SaveAction->setIcon(QIcon("./assets/img/save button.png"));
        this->SaveAction->setShortcut(QKeySequence("Ctrl+S"));
        connect(this->SaveAction, SIGNAL(triggered()), this, SLOT(save()));
        FileMenu->addAction(this->SaveAction);

        QAction *QuitAction = new QAction(QObject::tr("&Exit"), this);
        QuitAction->setIcon(QIcon("./assets/img/quit button.png"));
        QuitAction->setShortcut(QKeySequence("Ctrl+Q"));
        connect(QuitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
        FileMenu->addAction(QuitAction);
    QMenu *GameMenu = menuBar()->addMenu(QObject::tr("&Game"));
        this->GenerateBattleAction = new QAction(QObject::tr("&Generate Battle"), this);
        this->GenerateBattleAction->setIcon(QIcon("./assets/img/generate battle.png"));
        this->GenerateBattleAction->setShortcut(QKeySequence("Ctrl+G"));
        connect(this->GenerateBattleAction, SIGNAL(triggered()), this, SLOT(generateBattle()));
        this->GenerateBattleAction->setEnabled(false);//tmp
        this->isInBattle = true;
        GameMenu->addAction(GenerateBattleAction);

        QLabel *timeoutLabel = new QLabel(QObject::tr(" timeout : "));
        this->timeout = new QSpinBox;
        this->timeout->setRange(1, 10000);
        this->timeout->setValue(100);

        QLabel *sleeptimeLabel = new QLabel(QObject::tr(" sleep time : "));
        this->sleeptime = new QSpinBox;
        this->sleeptime->setRange(1, 1000);
        this->sleeptime->setValue(20);

        this->continueGame = new QCheckBox(QObject::tr(" continue :"));
        this->continueGame->setLayoutDirection(Qt::RightToLeft);
        this->continueGame->setChecked(false);

        QLabel *zoomLabel = new QLabel(QObject::tr(" zoom : "));
        QSpinBox *zoom = new QSpinBox;
        zoom->setRange(75, 150);
        zoom->setValue(100);
        this->scale = 1;
        QObject::connect(zoom, SIGNAL(valueChanged(int)), this, SLOT(zoom(int)));

    QToolBar *fileToolbar = addToolBar("FileToolbar");
        fileToolbar->addAction(QuitAction);
        fileToolbar->addAction(this->NewBattleAction);
        fileToolbar->addAction(this->OpenAction);
        fileToolbar->addAction(this->SaveAction);
        fileToolbar->addAction(this->GenerateBattleAction);
        fileToolbar->addWidget(timeoutLabel);
        fileToolbar->addWidget(timeout);
        fileToolbar->addWidget(sleeptimeLabel);
        fileToolbar->addWidget(sleeptime);
        fileToolbar->addWidget(this->continueGame);
        fileToolbar->addWidget(zoomLabel);
        fileToolbar->addWidget(zoom);
}

void MainWindow::save ()
{
    QString fileName = QFileDialog::getSaveFileName(this, QObject::tr("Save Battle"), QString("./save/"));// tmp "./assets/prog/"
    if(!fileName.isEmpty()) {
        QFile file(fileName);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream stream(&file);
        QDomDocument doc("xml");
         QDomElement root = doc.createElement("xml");
         doc.appendChild(root);

         QDomElement gameInfos = doc.createElement("gameInfos");
         root.appendChild(gameInfos);
             QDomElement timeout = doc.createElement("timeout");
             gameInfos.appendChild(timeout);
             timeout.appendChild(doc.createTextNode(QString("%1").arg(this->timeout->value())));

             QDomElement sleeptime = doc.createElement("sleeptime");
             gameInfos.appendChild(sleeptime);
             sleeptime.appendChild(doc.createTextNode(QString("%1").arg(this->sleeptime->value())));

             QDomElement continueGame = doc.createElement("continueGame");
             gameInfos.appendChild(continueGame);
             continueGame.appendChild(doc.createTextNode(QString("%1").arg(this->continueGame->isChecked())));

             QDomElement playerNumber = doc.createElement("playerNumber");
             gameInfos.appendChild(playerNumber);
             playerNumber.appendChild(doc.createTextNode(QString("%1").arg(this->playerNumber)));

         QDomElement players = doc.createElement("players");
         root.appendChild(players);

         for (int i = 0; i < this->playerNumber; ++i) {
             PlayerWidget *player = this->players[i];

             QDomElement playerElement = doc.createElement("player");
             players.appendChild(playerElement);

                 QDomElement id = doc.createElement("id");
                 playerElement.appendChild(id);
                 id.appendChild(doc.createTextNode(QString("%1").arg(i)));

                 QDomElement program = doc.createElement("program");
                 playerElement.appendChild(program);
                 program.appendChild(doc.createTextNode(player->programDirectory + "/" + player->programName));

                 QDomElement color = doc.createElement("color");
                 playerElement.appendChild(color);
                     QDomElement red = doc.createElement("red");
                     color.appendChild(red);
                     red.appendChild(doc.createTextNode(QString("%1").arg(player->color.red())));

                     QDomElement green = doc.createElement("green");
                     color.appendChild(green);
                     green.appendChild(doc.createTextNode(QString("%1").arg(player->color.green())));

                     QDomElement blue = doc.createElement("blue");
                     color.appendChild(blue);
                     blue.appendChild(doc.createTextNode(QString("%1").arg(player->color.blue())));

                 if (player->isScript->isChecked()) {
                     QDomElement script = doc.createElement("script");
                     playerElement.appendChild(script);
                     script.appendChild(doc.createTextNode(player->script->text()));
                 }

                 if (!player->randomPosition->isChecked()) {
                     QDomElement position = doc.createElement("position");
                     playerElement.appendChild(position);
                         QDomElement x = doc.createElement("x");
                         position.appendChild(x);
                         x.appendChild(doc.createTextNode(QString("%1").arg(player->posX->value())));

                         QDomElement y = doc.createElement("y");
                         position.appendChild(y);
                         y.appendChild(doc.createTextNode(QString("%1").arg(player->posY->value())));
                 }
         }

         QDomElement rounds = doc.createElement("rounds");
         root.appendChild(rounds);

         for (int i = 0; i < this->rounds.size(); ++i) {
             TronRound *round = &(this->rounds[i]);

             QDomElement roundElement = doc.createElement("round");
             rounds.appendChild(roundElement);

                 QDomElement lapNumber = doc.createElement("lapNumber");
                 roundElement.appendChild(lapNumber);
                 lapNumber.appendChild(doc.createTextNode(QString("%1").arg(i)));

                 QDomElement playerId = doc.createElement("playerId");
                 roundElement.appendChild(playerId);
                 playerId.appendChild(doc.createTextNode(QString("%1").arg(round->playerId)));

                 QDomElement departure = doc.createElement("departure");
                 roundElement.appendChild(departure);
                     QDomElement x = doc.createElement("x");
                     departure.appendChild(x);
                     x.appendChild(doc.createTextNode(QString("%1").arg(round->departure.x)));

                     QDomElement y = doc.createElement("y");
                     departure.appendChild(y);
                     y.appendChild(doc.createTextNode(QString("%1").arg(round->departure.y)));

                 QDomElement movement = doc.createElement("movement");
                 roundElement.appendChild(movement);
                 movement.appendChild(doc.createTextNode(round->movement.toString().c_str()));

                 QDomElement justLost = doc.createElement("justLost");
                 roundElement.appendChild(justLost);
                 justLost.appendChild(doc.createTextNode(QString("%1").arg(round->justLost)));

                 QDomElement stdout = doc.createElement("stdout");
                 roundElement.appendChild(stdout);
                 stdout.appendChild(doc.createTextNode(round->stdout.c_str()));

                 QDomElement stderr = doc.createElement("stderr");
                 roundElement.appendChild(stderr);
                 stderr.appendChild(doc.createTextNode(round->stderr.c_str()));

                 if (round->justLost) {
                     QDomElement lostMessage = doc.createElement("lostMessage");
                     roundElement.appendChild(lostMessage);
                     lostMessage.appendChild(doc.createTextNode(round->lostMessage.c_str()));
                 }
         }

         QString xml = doc.toString();

        stream << xml;
        file.close();
    }
}

void MainWindow::open ()
{
    QString fileName = QFileDialog::getOpenFileName(this, QObject::tr("Open Battle"), QString("./save/"));// tmp "./assets/prog/"
    if(!fileName.isEmpty()) {

        QDomDocument doc("xml");
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly))
            return;
        if (!doc.setContent(&file)) {
            file.close();
            return;
        }
        file.close();

        this->clearGame();

        QDomElement docElem = doc.documentElement();

        QDomNode n = docElem.firstChild();
        while(!n.isNull()) {
            QDomElement e = n.toElement();
            if(!e.isNull()) {
                if (e.tagName() == "gameInfos") {
                    QDomNode info = e.firstChild();
                    while(!info.isNull()) {
                        QDomElement infoElement = info.toElement();
                        if(!infoElement.isNull()) {
                            if (infoElement.tagName() == "timeout") {
                                this->timeout->setValue(infoElement.firstChild().toText().data().toInt());
                            }
                            else if (infoElement.tagName() == "sleeptime") {
                                this->sleeptime->setValue(infoElement.firstChild().toText().data().toInt());
                            }
                            else if (infoElement.tagName() == "continueGame") {
                                this->continueGame->setChecked(infoElement.firstChild().toText().data().toInt());
                            }
                            else if (infoElement.tagName() == "playerNumber") {
                                this->setPlayerNumber(infoElement.firstChild().toText().data().toInt());
                            }
                        }
                        info = info.nextSibling();
                    }
                }
                else if (e.tagName() == "players") {
                    QDomNode player = e.firstChild();
                    short id = 0;
                    while(!player.isNull()) {
                        QDomElement playerElement = player.toElement();
                        if(!playerElement.isNull()) {
                            if (playerElement.tagName() == "player") {
                                QDomNode info = playerElement.firstChild();

                                this->players[id]->isScript->setChecked(false);
                                this->players[id]->randomPosition->setChecked(true);

                                while(!info.isNull()) {
                                    QDomElement infoElement = info.toElement();
                                    if(!infoElement.isNull()) {
                                        if (infoElement.tagName() == "program") {
                                            this->players[id]->setProgramName(infoElement.firstChild().toText().data());
                                        }
                                        else if (infoElement.tagName() == "color") {
                                            QColor color;
                                            color.setRed(infoElement.elementsByTagName("red").item(0).firstChild().toText().data().toInt());
                                            color.setGreen(infoElement.elementsByTagName("green").item(0).firstChild().toText().data().toInt());
                                            color.setBlue(infoElement.elementsByTagName("blue").item(0).firstChild().toText().data().toInt());
                                            this->players[id]->setPlayerColor(color);
                                        }
                                        else if (infoElement.tagName() == "script") {
                                            this->players[id]->isScript->setChecked(true);
                                            this->players[id]->script->setText(infoElement.firstChild().toText().data());
                                        }
                                        else if (infoElement.tagName() == "position") {
                                            this->players[id]->randomPosition->setChecked(false);
                                            this->players[id]->posX->setValue(infoElement.elementsByTagName("x").item(0).firstChild().toText().data().toInt());
                                            this->players[id]->posY->setValue(infoElement.elementsByTagName("y").item(0).firstChild().toText().data().toInt());
                                        }
                                    }
                                    info = info.nextSibling();
                                }

                                id++;
                            }
                        }
                        player = player.nextSibling();
                    }
                }
                else if (e.tagName() == "rounds") {
                    QDomNode round = e.firstChild();
                    short id = 0;
                    this->rounds.clear();

                    while(!round.isNull()) {
                        QDomElement roundElement = round.toElement();
                        if(!roundElement.isNull()) {
                            if (roundElement.tagName() == "round") {
                                QDomNode info = roundElement.firstChild();
                                short playerId;
                                Board::Coordinate _departure;
                                Board::Direction _movement;
                                std::string _stdout, _stderr, _lostMessage;
                                bool _justLost = false;

                                while(!info.isNull()) {
                                    QDomElement infoElement = info.toElement();
                                    if(!infoElement.isNull()) {
                                        if (infoElement.tagName() == "playerId") {
                                            playerId = infoElement.firstChild().toText().data().toInt();
                                        }
                                        else if (infoElement.tagName() == "departure") {
                                            _departure.x = infoElement.elementsByTagName("x").item(0).firstChild().toText().data().toInt();
                                            _departure.y = infoElement.elementsByTagName("y").item(0).firstChild().toText().data().toInt();
                                        }
                                        else if (infoElement.tagName() == "movement") {
                                            _movement = infoElement.firstChild().toText().data().toStdString();
                                        }
                                        else if (infoElement.tagName() == "justLost") {
                                            _justLost = infoElement.firstChild().toText().data().toInt();
                                        }
                                        else if (infoElement.tagName() == "stdout") {
                                            _stdout = infoElement.firstChild().toText().data().toStdString();
                                        }
                                        else if (infoElement.tagName() == "stderr") {
                                            _stderr = infoElement.firstChild().toText().data().toStdString();
                                        }
                                        else if (infoElement.tagName() == "lostMessage") {
                                            _lostMessage = infoElement.firstChild().toText().data().toStdString();
                                        }
                                    }
                                    info = info.nextSibling();
                                }

                                this->rounds.push_back(TronRound(playerId, _departure, _movement, _stdout, _stderr, _lostMessage, _justLost));
                            }
                        }
                        round = round.nextSibling();
                    }
                }


            }
            n = n.nextSibling();
        }

        this->setInBattle ();
    }
}

void MainWindow::zoom (int percent)
{
    double newScale = (double) percent / 100;
    this->view->setFixedSize(604 * newScale, 404 * newScale);
    this->view->scale(1/this->scale, 1/this->scale);
    this->view->scale(newScale, newScale);
    this->scale = newScale;
}

void MainWindow::initGameMenu ()
{
    this->gameLayout = new QVBoxLayout();
    this->initGameScene();
    this->initSlider();
    this->initTimer();
    this->initConsole();
    this->centralLayout->addLayout(this->gameLayout);
}

void MainWindow::initGameScene ()
{
    this->scene = new QGraphicsScene();
    this->scene->setBackgroundBrush(QBrush(QColor(128, 128, 128)));

    this->view = new QGraphicsView(this->scene);
    this->view->setFixedSize(604, 404); // tmp
    //this->view->setMinimumSize(302, 202);
    //this->view->setMaximumSize(906, 606);
    //this->view->scale(1.5, 1.5);

    QPen pen1(QColor(64, 64, 64), 1, Qt::SolidLine);
    this->scene->addLine(QLine(0, 0, 0, 400), pen1);
    this->scene->addLine(QLine(600, 0, 600, 400), pen1);
    this->scene->addLine(QLine(0, 0, 600, 0), pen1);
    this->scene->addLine(QLine(0, 400, 600, 400), pen1);

    QPen pen(QColor(96, 96, 96), 2, Qt::SolidLine);
    pen1.setWidth(2);

    for(int i = 0; i < 29; ++i) {
        QLine ligne(i*20 + 19, 1, i*20 + 19, 399);
        this->scene->addLine(ligne, pen);
        this->scene->addLine(ligne, pen);
    }

    for(int j = 0; j < 19; ++j) {
        QLine ligne(1,j*20 + 19, 599, j*20 + 19);
        this->scene->addLine(ligne, pen);
    }

    for(int i = 4; i < 29; i+=5) {
        QLine ligne(i*20 + 19, 1, i*20 + 19, 399);
        this->scene->addLine(ligne, pen);
        this->scene->addLine(ligne, pen1);
    }

    for(int j = 4; j < 19; j+=5) {
        QLine ligne(1,j*20 + 19, 599, j*20 + 19);
        this->scene->addLine(ligne, pen1);
    }

    QHBoxLayout *viewLayout = new QHBoxLayout();
    this->gameLayout->addLayout((viewLayout));
    viewLayout->addWidget(this->view);
}

void MainWindow::cleanGameScene ()
{
    for (int i = 0; i < 4; ++i) {
        while (this->PlayersLigthCycles[i].size() > 0) {
            this->PlayersLigthCycles[i].pop_back();
        }
    }
    while (this->ligthCycles.size() > 0) {
        this->scene->removeItem(this->ligthCycles.back());
        delete this->ligthCycles.back();
        this->ligthCycles.pop_back();
    }
}

void MainWindow::initTimer()
{
    this->timer = new QTimer();
    QObject::connect(this->timer, SIGNAL(timeout()),this, SLOT(play()));
    this->timer->setInterval(40); // tmp 100
}

void MainWindow::initSlider ()
{
    this->sliderLayout = new QHBoxLayout();
    this->startButton = new QPushButton(QIcon("assets/img/start button.png"), "");
    this->startButton->setToolTip("Play");
    this->startButton->setShortcut(QKeySequence("Ctrl+P"));
    this->sliderLayout->addWidget(this->startButton);

    this->gameViewSlider = new QSlider(Qt::Horizontal);
    this->gameViewSlider->setRange(1, 1);
    this->sliderLayout->addWidget(this->gameViewSlider);
    this->sliderLabel = new QLabel();
    this->sliderLayout->addWidget(this->sliderLabel);
    this->gameLayout->addLayout(this->sliderLayout);

    this->regenerateButton = new QPushButton(QIcon("assets/img/reload button.png"), "");
    this->regenerateButton->setToolTip("Regenerate end of battle");
    this->regenerateButton->setShortcut(QKeySequence("Ctrl+R"));
    this->sliderLayout->addWidget(this->regenerateButton);

    QObject::connect(this->gameViewSlider, SIGNAL(valueChanged(int)), this, SLOT(onSliderChanged(int)));
    QObject::connect(this->startButton, SIGNAL(clicked()), this, SLOT(onPlayButtonClicked()));
    QObject::connect(this->regenerateButton, SIGNAL(clicked()), this, SLOT(regenerateBattle()));
}

void MainWindow::initConsole ()
{
    this->console = new QTextEdit();
    this->console->setReadOnly(true);
    this->console->setFontFamily("Monospace");
    this->console->setMinimumHeight(200);
    this->setDefaultConsoleStyle();
    this->gameLayout->addWidget(this->console);
}

void MainWindow::setDefaultConsoleStyle ()
{
    this->console->setStyleSheet(QString::fromUtf8("background-color: rgb(32, 32, 32);"));
    this->console->setTextColor(QColor(200, 200, 200));
    this->console->setTextBackgroundColor(QColor(32, 32, 32));
}

void MainWindow::initPlayerMenu ()
{
    this->playersWidget = new QWidget(this);
    this->playersWidget->setFixedWidth(200);
    QVBoxLayout * playerLayout = new QVBoxLayout(this->playersWidget);//tmp pointeurs
        this->players[0] = new PlayerWidget(0, QColor(76, 186, 255));
        this->players[1] = new PlayerWidget(1, QColor(215, 194, 57));
        this->players[2] = new PlayerWidget(2, QColor(15, 163, 101));
        this->players[3] = new PlayerWidget(3, QColor(229, 71, 71));

        for (int i = 0; i < 4; ++i) {
            playerLayout->addWidget(this->players[i]);
            QObject::connect(this->players[i], SIGNAL(colorChanged(QColor,short)), this, SLOT(onColorChanged(QColor,short)));
            QObject::connect(this->players[i], SIGNAL(positionChanged()), this, SLOT(onPlayerPostionChanged()));
            if (i >= this->playerNumber) {
                this->players[i]->setHidden(true);
            }
        }

        this->playerManagerLayout = new QHBoxLayout();

        this->addPlayerButton = new QPushButton("+");
        this->addPlayerButton->setMaximumSize(30, 30);
        this->removePlayerButton = new QPushButton("-");
        this->removePlayerButton->setMaximumSize(20, 20);
        this->playerManagerLayout->addWidget(this->addPlayerButton);
        this->playerManagerLayout->addWidget(this->removePlayerButton);
        playerLayout->addLayout(this->playerManagerLayout);
        QObject::connect(this->addPlayerButton, SIGNAL(clicked()), this, SLOT(addPlayer()));
        QObject::connect(this->removePlayerButton, SIGNAL(clicked()), this, SLOT(removePlayer()));

   this->centralLayout->addWidget(this->playersWidget);
   this->disablePlayersWidget(false); // tmp
}

QString MainWindow::getBGColorStyleSheet (QColor color) {
    std::stringstream styleSheet;
    styleSheet << "background-color: rgb(" << color.red() << ", " << color.green() << ", " << color.blue() << ");";
    return styleSheet.str().c_str();
}

void MainWindow::onPlayButtonClicked ()
{
    this->setPlaying(!this->playing);
    if (this->playing) {
        this->timer->start(this->sleeptime->value() / this->playerNumber); // tmp 100
    }
}

void MainWindow::setPlaying (bool _playing)
{
    this->playing = _playing;
    if (this->playing) {
        this->startButton->setIcon(QIcon("assets/img/pause button.png"));
        this->regenerateButton->setDisabled(true);
    }
    else {
        this->timer->stop();
        this->startButton->setIcon(QIcon("assets/img/start button.png"));
        this->regenerateButton->setDisabled(false);//tmp ??
    }
}

void MainWindow::onSliderChanged (int value)
{
    if (this->isInBattle) {
        if (value != this->currentRound + 1) {
            this->setPlaying(false);
        }
        this->displayRound(value, this->currentRound);
        this->currentRound = value;
        this->setSliderLabel(value);
    }
    else {
        this->sliderLabel->setText("");
    }
}

void MainWindow::setSliderLabel (int value)
{
    this->currentRound = value;
    std::stringstream s;
    s << value << " / " << this->rounds.size();
    this->sliderLabel->setText(s.str().c_str());
}

void MainWindow::onColorChanged (const QColor & color, short id)
{
    QPen pen(color, 12, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    for (int j = 0; j < this->PlayersLigthCycles[id].size(); ++j) {
        this->PlayersLigthCycles[id][j]->setPen(pen);
    }
}

void MainWindow::disablePlayersWidget (bool disable)
{
    for (int i = 0; i < 4; i++) {
        this->players[i]->disable(disable);
    }

    this->addPlayerButton->setDisabled(disable || this->playerNumber == 4);
    this->removePlayerButton->setDisabled(disable || this->playerNumber == 1);
}

void MainWindow::addPlayer ()
{
    if (this->playerNumber < 4) {
        this->playerNumber++;
        this->removePlayerButton->setDisabled(false);//tmp
        this->addPlayerButton->setDisabled(playerNumber == 4);//tmp
        this->players[this->playerNumber - 1]->setHidden(false);
        this->onPlayerPostionChanged();
    }
}

void MainWindow::removePlayer ()
{
    if (this->playerNumber > 1) {
        this->playerNumber--;
        this->addPlayerButton->setDisabled(false);//tmp
        this->removePlayerButton->setDisabled(playerNumber == 1);//tmp
        this->players[this->playerNumber]->setHidden(true);
        this->onPlayerPostionChanged();
    }
}

void MainWindow::onPlayerPostionChanged ()
{
    this->checkPlayerPosition();
    /*/if (this->checkPlayerPosition()) {//tmp
        this->GenerateBattleAction->setDisabled(false);
    }
    else {
        this->GenerateBattleAction->setDisabled(true);
    }*/
}

bool MainWindow::checkPlayerPosition () // tmp call this when a new player is added
{
    bool ok = true;
    for (int i = 0; i < this->playerNumber; ++i) {
        this->players[i]->setPositionBackgroundColor();
    }
    for (int i = 0; i < this->playerNumber - 1; ++i) {
        if (!this->players[i]->randomPosition->isChecked()) {
            for (int j = i + 1; j < this->playerNumber; ++j) {
                if (!this->players[j]->randomPosition->isChecked() &&
                        this->players[i]->posX->value() == this->players[j]->posX->value() && this->players[i]->posY->value() == this->players[j]->posY->value()) {
                    this->players[i]->setPositionBackgroundColor(QColor(249, 91, 91));
                    this->players[j]->setPositionBackgroundColor(QColor(249, 91, 91));
                    ok = false;
                }
            }
        }
    }

    return ok;
}

void MainWindow::enableBattleSettings ()
{
    this->disablePlayersWidget(false);
    this->NewBattleAction->setDisabled(true);
    this->SaveAction->setDisabled(true);
    this->GenerateBattleAction->setDisabled(false);
    this->gameViewSlider->setDisabled(true);
    this->startButton->setDisabled(true);
    this->regenerateButton->setDisabled(true);
    this->clearGame();
}

void MainWindow::clearGame ()
{
    this->isInBattle = false;
    this->cleanGameScene();
    this->gameViewSlider->setRange(1, 1);
    this->console->clear();
}

void MainWindow::newBattle () //tmp
{
    //int newBattle = QMessageBox::question(this, QObject::tr("New Battle"), QObject::tr("Are you sure you want to create a new battle ?"), QMessageBox::Yes | QMessageBox::No);
    //if (newBattle == QMessageBox::Yes) {
        enableBattleSettings();
    //}
}

void MainWindow::generateBattle ()
{
    if (!this->isInBattle) {//tmp
        if(this->doGeneration()) {
            this->setInBattle();
        }
    }
}

void MainWindow::regenerateBattle ()
{
    if (this->isInBattle) {//tmp
        int actualRound = this->gameViewSlider->value();
        std::vector<TronRound> _rounds;

        for (int i = 0; i < actualRound; ++i) {
            _rounds.push_back(this->rounds[i]);
        }

        if(this->doGeneration(&_rounds)) {
            this->enableBattleSettings();// tmp ??
            this->setInBattle();
            this->gameViewSlider->setValue(actualRound);
        }
    }
}

void MainWindow::setInBattle () // diaplay origin
{
    this->isInBattle = true;
    this->disablePlayersWidget(true);
    this->NewBattleAction->setDisabled(false);
    this->SaveAction->setDisabled(false);
    this->GenerateBattleAction->setDisabled(true);
    this->gameViewSlider->setRange(1, this->rounds.size());
    this->currentRound = 0;
    this->onSliderChanged(1);
    this->gameViewSlider->setDisabled(false);
    this->startButton->setDisabled(false);
    this->regenerateButton->setDisabled(false || this->playing);
    //this->scene->addEllipse(100,100, 10, 10);
}

void MainWindow::initGame (short _playerNumber, char ***programs, int _sleeptime, int _timeout)//tmp sleep time && time out
{
    assert(_sleeptime > 0);
    assert(_timeout > 0);
    this->sleeptime->setValue(_sleeptime);
    this->timeout->setValue(_timeout);

    this->setPlayerNumber(_playerNumber);

    for (int i = 0; i < _playerNumber; ++i) {
        this->players[i]->setProgramName(programs[i][0]);//tmp [0]
    }
}

bool MainWindow::doGeneration (std::vector<TronRound> *_rounds)
{
    char ***programs_arg_lists = NULL;
    programs_arg_lists = new char**[this->playerNumber];

    for (int i = 0; i < this->playerNumber; ++i) {
        int argId = 0;
        if (this->players[i]->isScript->isChecked()) {
            programs_arg_lists[i] = new char*[3];
            programs_arg_lists[i][argId] = new char[255];
            strcpy(programs_arg_lists[i][argId++], this->players[i]->script->text().toStdString().c_str());
        }
        else {
            programs_arg_lists[i] = new char*[2];
        }

        programs_arg_lists[i][argId] = new char[255];
        strcpy(programs_arg_lists[i][argId++], (this->players[i]->programDirectory + "/" + this->players[i]->programName).toStdString().c_str());
        programs_arg_lists[i][argId] = NULL;

        struct stat sb;

        if (stat((this->players[i]->programDirectory + "/" + this->players[i]->programName).toStdString().c_str(), &sb) == -1) {
            QMessageBox::warning(this, QObject::tr("Warning"), QObject::tr("Program \"") + this->players[i]->programDirectory + "/" + this->players[i]->programName + QObject::tr("\" does not exist !"));
            printf("Program \"%s/%s\" does not exist !", this->players[i]->programDirectory.toAscii().data(), this->players[i]->programName.toAscii().data());
            return false;
        }
        else if (!(sb.st_mode & S_IXGRP) && !this->players[i]->isScript->isChecked()) {
            QMessageBox::warning(this, QObject::tr("Warning"), QObject::tr("Program \"") + this->players[i]->programDirectory + "/" + this->players[i]->programName + QObject::tr("\" is not executable !"));
            return false;
        }
    }

    this->game = new TronGame(30, 20, this->playerNumber, programs_arg_lists, this->continueGame->isChecked(), 0, this->timeout->value(), false);//tmp timeout

    for (int i = 0; i < this->playerNumber; ++i) {
        if (_rounds == NULL) {
            if (!this->players[i]->randomPosition->isChecked()) {
                this->game->lightCycles[i]->initPosition(this->players[i]->posX->value(), this->players[i]->posY->value());
            }
        }
        else if (i < this->rounds.size()) {
            this->game->lightCycles[i]->initPosition(this->rounds[i].departure.x, this->rounds[i].departure.y);
        }
    }

    this->rounds = this->game->play(_rounds);
    delete this->game;
    for (int i  = 0; i < this->playerNumber; ++i) {
        for (int j = 0; programs_arg_lists[i][j] != NULL; ++j) {
            delete[] programs_arg_lists[i][j];
        }
        delete[] programs_arg_lists[i];
    }
    delete[] programs_arg_lists;

    return true;
}

void MainWindow::play ()
{
    if (this->currentRound >= this->rounds.size()) {
        this->setPlaying(false);
    }
    else {
        this->gameViewSlider->setValue(this->currentRound + 1);
    }
}

void MainWindow::displayRound(int round, int currentRound) {
    if (round > currentRound) {

        for (int i = currentRound; i < round; ++i) {
            short playerId = this->rounds[i].playerId;
            if (! this->rounds[i].justLost) {
                QPen pen(this->players[playerId]->color, 12, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
                Board::Coordinate departure = this->rounds[i].departure;
                Board::Coordinate arrival = this->rounds[i].arrival;
                QLine line((departure.x - 1)*20 + 9,(departure.y - 1)*20 + 9, (arrival.x - 1)*20 + 9,(arrival.y - 1)*20 + 9);
                this->ligthCycles.push_back(this->scene->addLine(line, pen));
                this->PlayersLigthCycles[playerId].push_back(this->ligthCycles.back());
            }
            else {
                for (int j = 0; j < this->PlayersLigthCycles[playerId].size(); ++j) {
                    this->PlayersLigthCycles[playerId][j]->setOpacity(0);
                }
            }
        }
    }
    else if(round < currentRound) {
        for (int i = currentRound - 1; i >= round; --i) {
            short playerId = this->rounds[i].playerId;

            if (! this->rounds[i].justLost) {
                this->scene->removeItem(this->ligthCycles.back());
                delete this->ligthCycles.back();
                this->ligthCycles.pop_back();
                this->PlayersLigthCycles[playerId].pop_back();
            }
            else {
                for (int j = 0; j < this->PlayersLigthCycles[playerId].size(); ++j) {
                    this->PlayersLigthCycles[playerId][j]->setOpacity(1);
                }
            }
        }
    }
    --round;
    this->console->clear();
    this->setDefaultConsoleStyle();
    this->console->setTextBackgroundColor(this->players[this->rounds[round].playerId]->color);
    this->console->setTextColor(QColor(32, 32, 32));
    this->console->append(this->players[this->rounds[round].playerId]->programName);
    this->setDefaultConsoleStyle();
    this->console->append(this->rounds[round].stdout.c_str());
    this->console->setTextColor(QColor(229, 71, 71));
    this->console->append(this->rounds[round].lostMessage.c_str());
    this->console->setTextColor(QColor(215, 121, 54));
    this->console->append(this->rounds[round].stderr.c_str());
    this->console->verticalScrollBar()->triggerAction(QScrollBar::SliderToMinimum);
}

void MainWindow::setPlayerNumber(short _playerNumber)
{
    if (_playerNumber > 0 && _playerNumber <= 4) {
        this->playerNumber = _playerNumber;
        this->addPlayerButton->setDisabled(this->isInBattle || this->playerNumber == 4);
        this->removePlayerButton->setDisabled(this->isInBattle || this->playerNumber == 1);

        for (int i = 0; i < 4; ++i) {
            if (i <_playerNumber) {
                this->players[i]->setHidden(false);
            }
            else {
                this->players[i]->setHidden(true);
            }
        }
    }
}
