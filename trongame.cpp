#include "trongame.h"

TronGame::TronGame (int _width, int _height, short _playerNumber, char ***programs, bool _continueGame, int _sleepTime, int _timeout, bool _verbose)
    : playerNumber(_playerNumber), playerRemaining(_playerNumber), continueGame(_continueGame), sleepTime(_sleepTime * 1000), timeout(_timeout), verbose(_verbose)
{
    this->board = new Board::GameBoard<short>(_width + 2, _height + 2);
    for(int y = 0; y < _height + 2; y++) {
        for (int x = 0; x < _width + 2; x++) {
            if (x == 0 || x == _width + 1 || y == 0 || y == _height + 1) {
                (*this->board)(x, y, 8); // tmp
            }
            else {
                (*this->board)(x, y, -1);
            }
        }
    }

    this->players = new Computer*[this->playerNumber];
    this->lightCycles = new Tron::LightCycle*[this->playerNumber];
    for (int i = 0; i < this->playerNumber; ++i) {
        this->players[i] = new Computer(programs[i]);
        this->lightCycles[i] = new Tron::LightCycle(i, this->board->content, this->board->width, this->board->height);
    }
}

TronGame::~TronGame()
{
    for (int i = 0; i < this->playerNumber; ++i) {
        delete this->players[i];
    }

    delete[] this->players;
    delete[] this->lightCycles;
    delete this->board;
}

std::vector<TronRound> TronGame::play (std::vector<TronRound> *_rounds)
{
    this->rounds.clear();
    this->playerRemaining = this->playerNumber;

    if (_rounds != NULL) {
        for (int i = 0; i < _rounds->size(); ++i) {
            this->doFakeRound((*_rounds)[i]);
            usleep(this->sleepTime);
        }

        this->rounds = *_rounds;

        if (!this->doLap(((*_rounds).back().playerId + 1) % this->playerNumber)) {
            return this->rounds;
        }
    }

    do {
        usleep(this->sleepTime);// tmp check args
    } while (this->doLap());

    return this->rounds;
}

void TronGame::doFakeRound (TronRound round) // tmp don't care of player how have allready loose // tmp bug : random position peut remettre un joueur sur une case occupé...
{
    this->initStdMessage();

    Board::Coordinate newCoordinate = round.arrival;
    this->players[round.playerId]->getNextMovement(this->playerNumber, round.playerId, this->lightCycles, &(this->stdout), &(this->stderr), &(this->lostMessage), /*this->timeout*/2000);//tmp timeout

    if (round.justLost) {
        this->lightCycles[round.playerId]->loose = true;
        this->playerRemaining--;

        for (unsigned int j = 0; j < this->lightCycles[round.playerId]->lightCycle.size(); ++j) {
            (*this->board)(this->lightCycles[round.playerId]->lightCycle[j], -1);
        }
    }
    else {
        (*this->board)(newCoordinate, round.playerId);
        this->lightCycles[round.playerId]->lightCycle.push_back(newCoordinate);
    }

    this->stdout = round.stdout;
    this->stderr = round.stderr;
    this->lostMessage = round.lostMessage;

    if (this->verbose) {
        system("clear");
        this->display(round.playerId);
    }
}

bool TronGame::doLap (int i)
{
    for (i; i < this->playerNumber; ++i) {
        if (!this->lightCycles[i]->loose) {
            this->initStdMessage();
            Board::Coordinate actualCoordinate = this->lightCycles[i]->lightCycle.back();
            Board::Coordinate newCoordinate;
            Board::Direction nextMovement = this->players[i]->getNextMovement(this->playerNumber, i, this->lightCycles, &(this->stdout), &(this->stderr), &(this->lostMessage), this->timeout);
            newCoordinate = actualCoordinate + nextMovement;
            if ((*this->board)(newCoordinate) != -1) {
                this->lightCycles[i]->loose = true;
                this->playerRemaining--;

                if (nextMovement != Board::FIXED) {
                    std::stringstream s;
                    s << "ERROR: You can't go " <<  nextMovement.toString() << ".\n";
                    this->lostMessage = s.str();
                }

                for (unsigned int j = 0; j < this->lightCycles[i]->lightCycle.size(); ++j) {
                    (*this->board)(this->lightCycles[i]->lightCycle[j], -1);
                }
            }
            else {
                (*this->board)(newCoordinate, i);
                this->lightCycles[i]->lightCycle.push_back(newCoordinate);
            }

            this->rounds.push_back(TronRound(i, actualCoordinate, nextMovement, this->stdout, this->stderr, this->lostMessage, this->lightCycles[i]->loose));

            if (this->verbose) {
                system("clear");
                this->display(i);
            }

            if(this->playerRemaining < 2 && !this->continueGame) {
                break;
            }
        }
    }

    return this->playerRemaining != 0 && (this->continueGame || playerRemaining > 1);
}

void TronGame::initStdMessage ()
{
    this->stderr = "";
    this->stdout = "";
    this->lostMessage = "";
}

void TronGame::display (int playerId) {
    for(int y = 0; y < this->board->height; ++y) {
        for (int x = 0; x < this->board->width; ++x) {
            if ((*this->board)(x, y) == -1) {
                std::cout << "  ";
            }
            else {
                std::cout << (*this->board)(x, y) << " ";
            }
        }
        std::cout << std::endl;
    }
    std::cout << "<<< Player " << playerId << " >>>" << std::endl;
    std::cout << this->stdout << std::endl;
    std::cout << this->lostMessage << std::endl << std::endl;
    std::cout << this->stderr << std::endl;

}
