#ifndef TRONROUND_H
#define TRONROUND_H

#include "Board/coordinate.h"
#include "Board/direction.h"
#include <vector>
#include <string>

class TronRound
{
public:
    short playerId;
    Board::Coordinate departure;
    Board::Coordinate arrival;
    Board::Direction movement;
    bool justLost;
    std::string stdout;
    std::string lostMessage;
    std::string stderr;

    //tmp stream

public:
    TronRound(short id, Board::Coordinate _departure, Board::Direction _movement, std::string _stdout, std::string _stderr, std::string _lostMessage, bool _justLost = false);
};

#endif // TRONROUND_H
